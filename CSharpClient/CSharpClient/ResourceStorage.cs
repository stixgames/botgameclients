﻿using System.Collections.Generic;

namespace CSharpClient
{
    public enum Resource
    {
        Wood,
        Stone,
        SkillPoint,
        Score,
    }

    public class ResourceStorage
    {
        public Dictionary<Resource, ulong> Resources = new Dictionary<Resource, ulong>();

        //public void Add(Resource resource, ulong value)
        //{
            
        //}

        public ulong GetResource(Resource resource)
        {
            if (Resources.ContainsKey(resource))
            {
                return Resources[resource];
            }
            return 0;
        }

        public string[] GetDescription()
        {
            if (Resources.Count == 0)
            {
                return new[] {"None"};
            }

            var list = new List<string>();
            foreach (var kv in Resources)
            {
                list.Add($"{kv.Key}: {kv.Value}");
            }
            return list.ToArray();
        }
    }
}

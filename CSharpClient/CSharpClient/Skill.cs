﻿using System;
using System.Linq;
using CSharpClient.Effects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CSharpClient
{
    [JsonConverter(typeof(SkillConverter))]
    public class Skill
    {
        public string Name;
        public IEffect Effect;

        /// <summary>
        /// null when there are unlimited
        /// </summary>
        public int? Uses;

        public ResourceStorage Costs;

        public int Chargeup;

        public int Cooldown;
        public int CooldownLeft;

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(Uses)}: {(Uses.HasValue ? Uses.ToString() : "Unlimited")}, {nameof(Chargeup)}: {Chargeup}, {nameof(Cooldown)}: {Cooldown}, {nameof(CooldownLeft)}: {CooldownLeft}";
        }

        public string[] GetDescription()
        {
            var description = new[]
            {
                $"Name: {Name}",
                $"Uses: {(Uses.HasValue ? Uses.ToString() : "Unlimited")}",
                $"Chargeup: {Chargeup}",
                $"Cooldown: {Cooldown}",
                $"Cooldown Left: {CooldownLeft}",
                "Costs:"
            };

            var costDescription = Costs.GetDescription();
            for (int i = 0; i < costDescription.Length; i++)
            {
                costDescription[i] = $"  {costDescription[i]}";
            }

            var effectDescription = Effect.GetDescription();
            for (int i = 0; i < effectDescription.Length; i++)
            {
                effectDescription[i] = $"  {effectDescription[i]}";
            }

            return description.Concat(costDescription).Append("Effect").Concat(effectDescription).ToArray();
        }
    }

    public class SkillConverter : JsonConverter<Skill>
    {
        public override void WriteJson(JsonWriter writer, Skill value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override Skill ReadJson(JsonReader reader, Type objectType, Skill existingValue, bool hasExistingValue,
            JsonSerializer serializer)
        {
            var skillObj = JToken.ReadFrom(reader);

            var skill = new Skill
            {
                Name = (string) skillObj["name"],
                Effect = GetEffect(skillObj["effect"]),
                Uses = (int?) skillObj["uses"],
                Costs = skillObj["costs"].ToObject<ResourceStorage>(serializer),
                Chargeup = (int) skillObj["chargeup"],
                Cooldown = (int)skillObj["cooldown"],
                CooldownLeft = (int)skillObj["cooldown_left"],
            };

            return skill;
        }

        private IEffect GetEffect(JToken token)
        {
            var type = (string) token["type"];
            var data = token["data"];

            IEffect effect;
            switch (type)
            {
                case "Idle":
                    effect = new Idle();
                    break;

                case "Movement":
                    effect = new Movement
                    {
                        Distance = (int) data["distance"],
                    };
                    break;

                case "Teleport":
                    effect = new Teleport
                    {
                        Distance = (int)data["distance"],
                    };
                    break;

                case "LineAttack":
                    effect = new LineAttack
                    {
                        Distance = (int)data["distance"],
                        Damage = (int)data["damage"],
                    };
                    break;

                case "LineBlock":
                    effect = new LineBlock
                    {
                        Distance = (int)data["distance"],
                    };
                    break;

                case "AoE":
                    effect = new AoE
                    {
                        Radius = (int) data["radius"],
                        Distance = (int)data["distance"],
                        Damage = (int) data["damage"],
                    };
                    break;

                case "Heal":
                    effect = new Heal
                    {
                        Amount = (int) data["amount"],
                    };
                    break;

                case "Build":
                    effect = new Build
                    {
                        Items = data["items"].Select(x => x.ToObject<Item>()).ToArray()
                    };
                    break;

                case "Learn":
                    effect = new Learn
                    {
                        Skills = data["skills"].Select(x => x.ToObject<Skill>()).ToArray()
                    };
                    break;

                case "Collect":
                    effect = new Collect();
                    break;

                case "Harvest":
                    effect = new Harvest
                    {
                        CanHarvest = data["can_harvest"].Select(x => x.ToObject<Resource>()).ToArray(),
                    };
                    break;

                default:
                    throw new ArgumentException();
            }

            return effect;
        }
    }

    namespace Effects
    {
        public interface IEffect
        {
            Vector2[] GetMovement(EntityAction action);
            Vector2[] GetBlock(EntityAction action);
            (Vector2, int)[] GetAttacks(EntityAction action);
            void ApplyPassive(Entity entity);

            ActionType GetActionType();

            /// <summary>
            /// Checks if the action is valid for this type of effect. It does not check if the selected skill actually uses this effect.
            /// </summary>
            /// <param name="action"></param>
            /// <returns></returns>
            bool IsValidAction(EntityAction action);

            string[] GetDescription();
        }

        public enum ActionType
        {
            Idle,
            Targeted,
            Directional,
            OmniDirectional
        }

        public class Idle : IEffect
        {
            public Vector2[] GetMovement(EntityAction action) => new Vector2[0];

            public Vector2[] GetBlock(EntityAction action) => new Vector2[0];

            public (Vector2, int)[] GetAttacks(EntityAction action) => new (Vector2, int)[0];

            public void ApplyPassive(Entity entity) { }

            public ActionType GetActionType()
            {
                return ActionType.Idle;
            }

            public bool IsValidAction(EntityAction action)
            {
                return true;
            }

            public string[] GetDescription()
            {
                return new[]
                {
                    "Idle",
                };
            }
        }

        public class Movement : IEffect
        {
            public int Distance;

            public Vector2[] GetMovement(EntityAction action) => new Vector2[0];

            public Vector2[] GetBlock(EntityAction action) => new Vector2[0];

            public (Vector2, int)[] GetAttacks(EntityAction action) => new(Vector2, int)[0];

            public void ApplyPassive(Entity entity) { }
            
            public ActionType GetActionType()
            {
                return ActionType.Directional;
            }

            public bool IsValidAction(EntityAction action)
            {
                return action.Direction.HasValue;
            }

            public string[] GetDescription()
            {
                return new[]
                {
                    $"Movement, Distance: {Distance}",
                };
            }
        }

        public class Teleport : IEffect
        {
            public int Distance;

            public Vector2[] GetMovement(EntityAction action) => new Vector2[0];

            public Vector2[] GetBlock(EntityAction action) => new Vector2[0];

            public (Vector2, int)[] GetAttacks(EntityAction action) => new(Vector2, int)[0];

            public void ApplyPassive(Entity entity) { }

            public ActionType GetActionType()
            {
                return ActionType.Targeted;
            }

            public bool IsValidAction(EntityAction action)
            {
                return action.Target?.Magnitude <= Distance;
            }

            public string[] GetDescription()
            {
                return new[]
                {
                    $"Teleport, Max Distance: {Distance}",
                };
            }
        }

        public class AoE : IEffect
        {
            public int Radius;
            public int Distance;
            public int Damage;

            public Vector2[] GetMovement(EntityAction action) => new Vector2[0];

            public Vector2[] GetBlock(EntityAction action) => new Vector2[0];

            public (Vector2, int)[] GetAttacks(EntityAction action) => new(Vector2, int)[0];

            public void ApplyPassive(Entity entity) { }

            public ActionType GetActionType()
            {
                return ActionType.Targeted;
            }

            public bool IsValidAction(EntityAction action)
            {
                return action.Target?.Magnitude <= Distance;
            }

            public string[] GetDescription()
            {
                return new[]
                {
                    "AoE",
                    $"  Radius: {Radius}",
                    $"  Distance: {Distance}",
                    $"  Damage: {Damage}",
                };
            }
        }

        public class LineAttack : IEffect
        {
            public int Distance;
            public int Damage;

            public Vector2[] GetMovement(EntityAction action) => new Vector2[0];

            public Vector2[] GetBlock(EntityAction action) => new Vector2[0];

            public (Vector2, int)[] GetAttacks(EntityAction action) => new(Vector2, int)[0];

            public void ApplyPassive(Entity entity) { }

            public ActionType GetActionType()
            {
                return ActionType.Directional;
            }

            public bool IsValidAction(EntityAction action)
            {
                return action.Direction.HasValue;
            }

            public string[] GetDescription()
            {
                return new[]
                {
                    "LineAttack",
                    $"  Distance: {Distance}",
                    $"  Damage: {Damage}",
                };
            }
        }

        public class LineBlock : IEffect
        {
            public int Distance;

            public Vector2[] GetMovement(EntityAction action) => new Vector2[0];

            public Vector2[] GetBlock(EntityAction action) => new Vector2[0];

            public (Vector2, int)[] GetAttacks(EntityAction action) => new(Vector2, int)[0];

            public void ApplyPassive(Entity entity) { }

            public ActionType GetActionType()
            {
                return ActionType.Directional;
            }

            public bool IsValidAction(EntityAction action)
            {
                return action.Direction.HasValue;
            }

            public string[] GetDescription()
            {
                return new[]
                {
                    $"LineBlock, Distance: {Distance}",
                };
            }
        }

        public class Heal : IEffect
        {
            public int Amount;

            public Vector2[] GetMovement(EntityAction action) => new Vector2[0];

            public Vector2[] GetBlock(EntityAction action) => new Vector2[0];

            public (Vector2, int)[] GetAttacks(EntityAction action) => new(Vector2, int)[0];

            public void ApplyPassive(Entity entity) { }

            public ActionType GetActionType()
            {
                return ActionType.Idle;
            }

            public bool IsValidAction(EntityAction action)
            {
                return true;
            }

            public string[] GetDescription()
            {
                return new[]
                {
                    $"Heal Amount: {Amount}",
                };
            }
        }

        public class Build : IEffect
        {
            public Item[] Items;

            public Vector2[] GetMovement(EntityAction action) => new Vector2[0];

            public Vector2[] GetBlock(EntityAction action) => new Vector2[0];

            public (Vector2, int)[] GetAttacks(EntityAction action) => new(Vector2, int)[0];

            public void ApplyPassive(Entity entity) { }

            public ActionType GetActionType()
            {
                return ActionType.Idle;
            }

            public bool IsValidAction(EntityAction action)
            {
                return true;
            }

            public string[] GetDescription()
            {
                var itemDescriptions = Items.SelectMany(x => x.GetDescription()).ToArray();

                Utilities.Indent(itemDescriptions);

                var description = new[]
                {
                    "Build Items:",
                };

                return description.Concat(itemDescriptions).ToArray();
            }
        }

        public class Learn : IEffect
        {
            public Skill[] Skills;

            public Vector2[] GetMovement(EntityAction action) => new Vector2[0];

            public Vector2[] GetBlock(EntityAction action) => new Vector2[0];

            public (Vector2, int)[] GetAttacks(EntityAction action) => new(Vector2, int)[0];

            public void ApplyPassive(Entity entity) { }

            public ActionType GetActionType()
            {
                return ActionType.Idle;
            }

            public bool IsValidAction(EntityAction action)
            {
                return true;
            }

            public string[] GetDescription()
            {
                var skillDescriptions = Skills.SelectMany(x => x.GetDescription()).ToArray();

                Utilities.Indent(skillDescriptions);

                var description = new[]
                {
                    $"Learn Skills:",
                };

                return description.Concat(skillDescriptions).ToArray();
            }
        }

        public class Collect : IEffect
        {
            public Vector2[] GetMovement(EntityAction action) => new Vector2[0];

            public Vector2[] GetBlock(EntityAction action) => new Vector2[0];

            public (Vector2, int)[] GetAttacks(EntityAction action) => new(Vector2, int)[0];

            public void ApplyPassive(Entity entity) { }

            public ActionType GetActionType()
            {
                return ActionType.Idle;
            }

            public bool IsValidAction(EntityAction action)
            {
                return true;
            }

            public string[] GetDescription()
            {
                return new[]
                {
                    "Collect resources at current position"
                };
            }
        }

        public class Harvest : IEffect
        {
            public Resource[] CanHarvest;

            public Vector2[] GetMovement(EntityAction action) => new Vector2[0];

            public Vector2[] GetBlock(EntityAction action) => new Vector2[0];

            public (Vector2, int)[] GetAttacks(EntityAction action) => new(Vector2, int)[0];

            public void ApplyPassive(Entity entity) { }

            public ActionType GetActionType()
            {
                return ActionType.OmniDirectional;
            }

            public bool IsValidAction(EntityAction action)
            {
                return true;
            }

            public string[] GetDescription()
            {
                var description = new[]
                {
                    "Harvest resources: "
                };

                var resourceNames = CanHarvest.Select(x => x.ToString()).ToArray();
                Utilities.Indent(resourceNames);

                return description.Concat(resourceNames).ToArray();
            }
        }
    }
}
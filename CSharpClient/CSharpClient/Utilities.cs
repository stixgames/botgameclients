﻿namespace CSharpClient
{
    public class Utilities
    {
        /// <summary>
        /// Modifies the input array and returns it, for chaining.
        /// </summary>
        /// <param name="killRewardDescriptions"></param>
        /// <returns></returns>
        public static string[] Indent(string[] killRewardDescriptions)
        {
            for (var i = 0; i < killRewardDescriptions.Length; i++)
            {
                killRewardDescriptions[i] = $"  {killRewardDescriptions[i]}";
            }

            return killRewardDescriptions;
        }
    }
}
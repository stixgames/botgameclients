﻿using System;
using Newtonsoft.Json;

namespace CSharpClient
{
    [JsonConverter(typeof(DirectionConverter))]
    public enum Direction
    {
        Up, Down, Left, Right
    }

    public class DirectionConverter : JsonConverter<Direction?>
    {
        public override void WriteJson(JsonWriter writer, Direction? value, JsonSerializer serializer)
        {
            switch (value)
            {
                case null:
                    writer.WriteValue((string)null);
                    break;
                case Direction.Up:
                    writer.WriteValue("Up");
                    break;
                case Direction.Down:
                    writer.WriteValue("Down");
                    break;
                case Direction.Left:
                    writer.WriteValue("Left");
                    break;
                case Direction.Right:
                    writer.WriteValue("Right");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }

        public override Direction? ReadJson(JsonReader reader, Type objectType, Direction? existingValue, bool hasExistingValue,
            JsonSerializer serializer)
        {
            var value = reader.Value as string;
            switch (value)
            {
                case "Up":
                    return Direction.Up;

                case "Down":
                    return Direction.Up;

                case "Left":
                    return Direction.Up;

                case "Right":
                    return Direction.Up;
            }

            return null;
        }
    }
}
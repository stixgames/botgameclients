﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace CSharpClient
{
    public class RequestResponseStream : IDisposable
    {
        private BinaryReader reader;
        private BinaryWriter writer;

        private JsonSerializer serializer;

        public RequestResponseStream(Stream stream)
        {
            serializer = JsonSerializer.Create(new JsonSerializerSettings
            {
                Formatting = Formatting.None,
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new SnakeCaseNamingStrategy(),
                },
            });

            reader = new BinaryReader(stream);
            writer = new BinaryWriter(stream);
        }

        public void Send(IRequest request)
        {
            var json = GenerateRequest(request);
            var jsonString = json.ToString(Formatting.None);

            SendRaw(jsonString);
        }

        public void Send(ICommand request)
        {
            var json = GenerateRequest(request);
            var jsonString = json.ToString(Formatting.None);

            SendRaw(jsonString);
        }

        private void SendRaw(string json)
        {
            var array = Encoding.UTF8.GetBytes(json);
            var compressed = Compress(array);

            writer.Write(compressed.Length);
            writer.Write(compressed);
        }

        /// <summary>
        /// Compresses byte array to new byte array.
        /// </summary>
        public static byte[] Compress(byte[] raw)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(memory,
                    CompressionMode.Compress, true))
                {
                    gzip.Write(raw, 0, raw.Length);
                }
                return memory.ToArray();
            }
        }

        public JObject ReadRaw()
        {
            var count = reader.ReadInt32();
            var compressed = reader.ReadBytes(count);
            var decompressed = Decompress(compressed);
            var jsonString = Encoding.UTF8.GetString(decompressed);

            var stringReader = new StringReader(jsonString);
            serializer.Deserialize<JObject>(new JsonTextReader(stringReader));

            return JObject.Parse(jsonString);
        }

        private static byte[] Decompress(byte[] gzip)
        {
            // Create a GZIP stream with decompression mode.
            // ... Then create a buffer and write into while reading from the GZIP stream.
            using (GZipStream stream = new GZipStream(new MemoryStream(gzip),
                CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    return memory.ToArray();
                }
            }
        }

        public Object Read()
        {
            var obj = ReadRaw();

            var response = obj.Properties().First();
            var responseType = ReflectionUtility.GetResponseTypes()[response.Name];
            var responseObject = response.Value.ToObject(responseType, serializer);

            return responseObject;
        }

        public T Read<T>()
        {
            return (T) Read();
        }

        private JObject GenerateRequest(ICommand command)
        {
            var commandObject = JToken.FromObject(command, serializer);
            JToken commandValue;

            if (command is IdleCommand)
            {
                commandValue = new JValue(command.CommandName);
            }
            else
            {
                commandValue = new JObject(new JProperty(command.CommandName, commandObject));
            }

            var fullRequest = 
                new JObject(
                    new JProperty("ExecuteCommand", commandValue));

            return fullRequest;
        }

        private JObject GenerateRequest(IRequest request)
        {
            var requestObject = JToken.FromObject(request, serializer);
            var fullRequest = new JObject(new JProperty(request.RequestName, requestObject));

            return fullRequest;
        }

        public void Dispose()
        {
            reader?.Dispose();
            writer?.Dispose();
        }
    }
}
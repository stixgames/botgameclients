﻿using Newtonsoft.Json;

namespace CSharpClient
{
    public interface ICommand
    {
        [JsonIgnore]
        string CommandName { get; }
    }

    public class SkillCommand : ICommand
    {
        public string CommandName => "Skill";

        /// <summary>
        /// The index of the skill you want to use
        /// </summary>
        public int SkillIndex;

        /// <summary>
        /// The direction of the skill
        /// </summary>
        public Direction? Direction;

        /// <summary>
        /// The target of the skill, as offset from the entity
        /// For example, the tile above and two to the right would be Vector2(-1, 2)
        /// </summary>
        public Vector2? Target;
    }

    public class IdleCommand : ICommand
    {
        public string CommandName => "Idle";
    }
}
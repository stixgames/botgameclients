﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CSharpClient
{
    public static class ReflectionUtility
    {
        private static Dictionary<string, Type> responseTypes;

        public static Dictionary<string, Type> GetResponseTypes()
        {
            if (responseTypes == null)
            {
                responseTypes =
                (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                    from type in assembly.GetTypes()
                    let attribute = (ResponseAttribute) Attribute.GetCustomAttribute(type, typeof(ResponseAttribute))
                    where attribute != null
                    select new {attribute.ResponseName, type}).ToDictionary(x => x.ResponseName, x => x.type);
            }

            return responseTypes;
        }
    }
}
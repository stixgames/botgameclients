﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using CSharpClient.Effects;

namespace CSharpClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new TcpClient("127.0.0.1", 8075);

            StartGame(client);

            Console.ReadLine();
        }

        private static void StartGame(TcpClient client)
        {
            using (var stream = new RequestResponseStream(client.GetStream()))
            {
                stream.Send(new ConnectionRequest("Name" + new Random().Next()));
                var response = stream.Read();

                if (response is PlayerAccepted playerAccepted)
                {
                    Console.WriteLine(playerAccepted);
                }
                else
                {
                    return;
                }

                HumanInterface.GameLoop(stream);

                //This will never be reached as long as the game loop exists.
                //First read the game state.
                var gameState = stream.Read<GameState>();

                //Look at your skills here, this includes item skills
                gameState.GetPlayer().GetAllSkills();

                //Example for moving the player up by one tile
                stream.Send(new SkillCommand {SkillIndex = 0, Direction = Direction.Up});

                //After each command you *must* read the new game area
                gameState = stream.Read<GameState>();

                //To understand how the game works, just try it in the human version,
                //it's basically a wrapper for all the commands, showing all the data the bot would have
            }
        }
    }
}
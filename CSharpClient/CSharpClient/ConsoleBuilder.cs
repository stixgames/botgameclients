﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpClient
{
    public class ConsoleBuilder
    {
        private List<List<(ConsoleColor, ConsoleColor)>> consoleColors = new List<List<(ConsoleColor, ConsoleColor)>>();
        private List<StringBuilder> builders = new List<StringBuilder>();
        private int line = 0;
        public int ColumnStart = 0;
        public int ColumnWidth = 0;

        public (ConsoleColor, ConsoleColor) DefaultColors = (ConsoleColor.White, ConsoleColor.Black);

        public void WriteLine()
        {
            WriteLine("");
        }

        public void WriteLine(string value)
        {
            if (ColumnWidth < value.Length)
            {
                value = value.Remove(ColumnWidth);
            }

            while (line >= builders.Count)
            {
                builders.Add(new StringBuilder());
            }

            var builder = builders[line];

            if (builder.Length > ColumnStart)
            {
                throw new InvalidOperationException($"The line is already longer than {nameof(ColumnStart)}");
            }

            var columnFiller = String.Concat(Enumerable.Repeat(" ", ColumnStart - builder.Length));
            builder.Append(columnFiller);
            builder.Append(value);

            UpdateConsoleColors();

            line++;
        }

        public void WriteLines(IEnumerable<string> lines)
        {
            foreach (var l in lines)
            {
                WriteLine(l);
            }
        }

        public void Append(string value)
        {
            while (line >= builders.Count)
            {
                builders.Add(new StringBuilder());
            }

            var builder = builders[line];

            if (builder.Length < ColumnStart)
            {
                var columnFiller = String.Concat(Enumerable.Repeat(" ", ColumnStart - builder.Length));
                builder.Append(columnFiller);
            }

            int removeIndex = ColumnStart + ColumnWidth - builder.Length;
            if (removeIndex < value.Length)
            {
                value = value.Remove(removeIndex);
            }
            builder.Append(value);

            UpdateConsoleColors();
        }

        public void AppendLine(string value)
        {
            Append(value);
            line++;
        }

        public void FillAllLines(int fillSize)
        {
            foreach (var builder in builders)
            {
                if (builder.Length < fillSize)
                {
                    var columnFiller = String.Concat(Enumerable.Repeat(" ", fillSize - builder.Length));
                    builder.Append(columnFiller);
                }
            }

            UpdateConsoleColors();
        }

        private void UpdateConsoleColors()
        {
            for (int i = 0; i < builders.Count; i++)
            {
                if (i >= consoleColors.Count)
                {
                    consoleColors.Add(new List<(ConsoleColor, ConsoleColor)>());
                }

                var builderLength = builders[i].Length;
                var colors = consoleColors[i];

                if (colors.Count < builderLength)
                {
                    colors.AddRange(Enumerable.Repeat((Console.ForegroundColor, Console.BackgroundColor), builderLength - colors.Count));
                }
            }
        }

        public void Clear()
        {
            ResetLineCursor();
            foreach (var builder in builders)
            {
                builder.Clear();
            }

            foreach (var list in consoleColors)
            {
                list.Clear();
            }
        }

        public void ResetLineCursor()
        {
            line = 0;
        }

        public void Print()
        {
            Console.OutputEncoding = Encoding.UTF8;

            //Clear console before drawing everything
            Console.SetCursorPosition(0, 0);

            //Draw everything with the default colors
            Console.ForegroundColor = DefaultColors.Item1;
            Console.BackgroundColor = DefaultColors.Item2;
            var complete = new StringBuilder();
            foreach (var builder in builders)
            {
                complete.AppendLine(builder.ToString());
            }
            Console.WriteLine(complete.ToString());

            //Write everything that doesn't have the default colors
            int lineIndex = 0;
            foreach (var line in consoleColors)
            {
                var builder = builders[lineIndex];

                int charIndex = 0;
                while (charIndex < line.Count)
                {
                    var colors = line[charIndex];

                    if (colors.Item1 != DefaultColors.Item1 || colors.Item2 != DefaultColors.Item2)
                    {
                        Console.SetCursorPosition(charIndex, lineIndex);
                        Console.ForegroundColor = colors.Item1;
                        Console.BackgroundColor = colors.Item2;

                        var b = new StringBuilder();
                        while (charIndex < line.Count && colors.Item1 == line[charIndex].Item1 ||
                               colors.Item2 == line[charIndex].Item2)
                        {
                            b.Append(builder[charIndex]);
                            charIndex++;
                        }

                        Console.Write(b);
                    }

                    charIndex++;
                }
                lineIndex++;
            }

            Console.ForegroundColor = DefaultColors.Item1;
            Console.BackgroundColor = DefaultColors.Item2;
        }
    }
}
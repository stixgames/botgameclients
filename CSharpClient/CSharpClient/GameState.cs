﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CSharpClient
{
    [Response("GameState")]
    public class GameState
    {
        public GameTile[] SurroundingArea;

        public Vector2 PlayerPos;

        /// <summary>
        /// The width and length of the area.
        /// </summary>
        public int AreaSize;

        public TileType[] TileData;

        public Entity GetPlayer()
        {
            return GetTile(PlayerPos).Entity;
        }

        public TileType GetTileType(Vector2 pos)
        {
            return GetTileType(pos.X, pos.Y);
        }

        public TileType GetTileType(int x, int y)
        {
            return TileData[GetTile(x, y).Tile.Id];
        }

        public GameTile GetTile(Vector2 pos)
        {
            return GetTile(pos.X, pos.Y);
        }

        public GameTile GetTile(int x, int y)
        {
            return SurroundingArea[x + y * AreaSize];
        }
    }

    public struct GameTile
    {
        public Tile Tile;
        public Entity Entity;
        public Dictionary<Resource, ulong> ResourceDrops;
        public Item[] ItemDrops;

        public string[] GetDescription(GameState gameState)
        {
            var tileDescription = Tile.GetDescription(gameState);

            var dropDescription = new List<string>();
            foreach (var kv in ResourceDrops)
            {
                dropDescription.Add($"{kv.Key}: {kv.Value}");
            }

            var itemDropDescriptions = new List<string>();
            foreach (var item in ItemDrops)
            {
                itemDropDescriptions.Add("Item Drop:");
                itemDropDescriptions.AddRange(Utilities.Indent(item.GetDescription()));
            }

            var entityDescription = Entity == null ? new[] { "No entity in this tile" } : Entity.GetDescription();

            var lines = new[]
                {
                    "Selection:",
                    "Tile:"
                }.Concat(tileDescription)
                .Append("")
                .Append("Dropped resources:")
                .Concat(dropDescription)
                .Append("")
                .Append("Dropped items:")
                .Concat(Utilities.Indent(itemDropDescriptions.ToArray()))
                .Append("")
                .Append("Entity:")
                .Concat(entityDescription)
                .ToArray();

            return lines;
        }
    }

    public struct Tile
    {
        public int Id;

        public string[] GetDescription(GameState gameState)
        {
            return gameState.TileData[Id].GetDescription();
        }
    }

    public class TileType
    {
        public String Name;
        public String MapSymbol;
        public bool IsBlocking;
        public ResourceStorage ResourcesPerHarvest;

        public string[] GetDescription()
        {
            return new[]
            {
                $"Name: {Name}",
                $"IsBlocking: {IsBlocking}",
                $"Resources per harvest:"
            }.Concat(Utilities.Indent(ResourcesPerHarvest.GetDescription())).ToArray();
        }
    }
}
﻿using Newtonsoft.Json;

namespace CSharpClient
{
    public interface IRequest
    {
        [JsonIgnore]
        string RequestName { get; }
    }

    public class ConnectionRequest : IRequest
    {
        public string name;

        public string RequestName => "Connection";

        public ConnectionRequest(string name)
        {
            this.name = name;
        }
    }
}
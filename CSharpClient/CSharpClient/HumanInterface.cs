﻿using System;
using System.Linq;
using CSharpClient.Effects;

namespace CSharpClient
{
    class HumanInterface
    {
        private const int MaxLines = 27;

        private enum InputMode
        {
            View,
            SelectSkill,
            EnterSkill,
        }

        public static void GameLoop(RequestResponseStream stream)
        {
            var builder = new ConsoleBuilder();

            //Game loop
            while (true)
            {
                var gameState = stream.Read<GameState>();
                var cursorPos = gameState.PlayerPos;
                var selectionScroll = 0;
                var inputMode = InputMode.View;
                var command = new SkillCommand();

                ICommand sendCommand;
                do
                {
                    Console.CursorVisible = false;

                    //Draw map
                    DrawMap(builder, gameState, cursorPos);

                    int selectionSize = DrawSelection(builder, gameState, cursorPos, selectionScroll, MaxLines);
                    selectionScroll = Math.Clamp(selectionScroll, 0, selectionSize - 1);

                    //Fill the rest of the console with whitespace
                    builder.FillAllLines(100);

                    //Print game view
                    builder.Print();
                    builder.Clear();

                    //Draw input area
                    sendCommand = Inputs(gameState, selectionSize, ref cursorPos, ref selectionScroll, ref inputMode, ref command);
                } while (sendCommand == null);

                stream.Send(sendCommand);
            }
        }

        private static ICommand Inputs(GameState gameState, int selectionSize, ref Vector2 cursorPos, ref int selectionScroll, ref InputMode mode, ref SkillCommand command)
        {
            Console.SetCursorPosition(0, MaxLines);
            Console.WriteLine(String.Concat(Enumerable.Repeat("_", 100)));
            Console.WriteLine(String.Concat(Enumerable.Repeat(" ", 100)));

            Console.SetCursorPosition(0, MaxLines+1);

            switch (mode)
            {
                case InputMode.View:
                    ViewMode(gameState, selectionSize, ref cursorPos, ref selectionScroll, ref mode);
                    break;
                case InputMode.SelectSkill:
                    Console.Write("Enter skill id, or a negative number to idle (wait for chargeup): ");
                    string input = Console.ReadLine();
                    if (string.IsNullOrWhiteSpace(input))
                    {
                        mode = InputMode.View;
                        break;
                    }

                    int id;
                    if (int.TryParse(input, out id))
                    {
                        if (id < 0)
                        {
                            return new IdleCommand();
                        }
                        if (id < gameState.GetPlayer().GetAllSkills().Length)
                        {
                            command.SkillIndex = id;
                            mode = InputMode.EnterSkill;
                        }
                    }
                    break;
                case InputMode.EnterSkill:
                    switch (gameState.GetPlayer().GetAllSkills()[command.SkillIndex].Effect.GetActionType())
                    {
                        case ActionType.Idle:
                            return command;
                        case ActionType.Targeted:
                            if (SelectTarget(gameState, ref cursorPos, ref command, ref mode))
                            {
                                return command;
                            }
                            break;
                        case ActionType.Directional:
                            Console.Write("Press the desired direction.");
                            command.Direction = GetDirectionInputLoop();
                            return command;
                        case ActionType.OmniDirectional:
                            Console.Write("Press the desired direction, or enter for your current position.");
                            command.Direction = GetOmniDirectionalInputLoop();
                            return command;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
            }

            return null;
        }

        private static bool SelectTarget(GameState gameState, ref Vector2 cursorPos, ref SkillCommand command, ref InputMode mode)
        {
            Console.WriteLine("Select target with direction keys or WASD, then press enter.");
            var input = Console.ReadKey(true);

            if (input.Key == ConsoleKey.Enter)
            {
                command.Target = cursorPos - gameState.GetPlayer().Pos;
                return true;
            }

            MoveCursor(gameState, input, ref cursorPos);

            return false;
        }

        private static void ViewMode(GameState gameState, int selectionSize, ref Vector2 cursorPos, ref int selectionScroll, ref InputMode mode)
        {
            Console.WriteLine("Press enter to select skill, use direction keys or WASD to move cursor, I/K to scroll.");

            var input = Console.ReadKey(true);

            switch (input.Key)
            {
                case ConsoleKey.I:
                    selectionScroll = selectionScroll - 1;
                    break;

                case ConsoleKey.K:
                    selectionScroll = selectionScroll + 1;
                    break;

                case ConsoleKey.Enter:
                    mode = InputMode.SelectSkill;
                    break;
            }

            MoveCursor(gameState, input, ref cursorPos);
        }

        private static void MoveCursor(GameState gameState, ref Vector2 cursorPos)
        {
            MoveCursor(gameState, Console.ReadKey(true), ref cursorPos);
        }

        private static void MoveCursor(GameState gameState, ConsoleKeyInfo input, ref Vector2 cursorPos)
        {
            var dir = GetDirectionInput(input.Key);
            if (dir != null)
            {
                cursorPos.AddDirection(dir.Value);
                cursorPos.Clamp(new Vector2(0, 0), new Vector2(gameState.AreaSize - 1, gameState.AreaSize - 1));
            }
        }

        private static Direction? GetOmniDirectionalInputLoop()
        {
            while (true)
            {
                var consoleKey = Console.ReadKey(true).Key;
                switch (consoleKey)
                {
                    case ConsoleKey.Enter:
                        return null;

                    case ConsoleKey.UpArrow:
                    case ConsoleKey.W:
                        return Direction.Up;

                    case ConsoleKey.DownArrow:
                    case ConsoleKey.S:
                        return Direction.Down;

                    case ConsoleKey.LeftArrow:
                    case ConsoleKey.A:
                        return Direction.Left;

                    case ConsoleKey.RightArrow:
                    case ConsoleKey.D:
                        return Direction.Right;
                }
            }
        }

        private static Direction GetDirectionInputLoop()
        {
            while (true)
            {
                var consoleKey = Console.ReadKey(true).Key;
                switch (consoleKey)
                {
                    case ConsoleKey.UpArrow:
                    case ConsoleKey.W:
                        return Direction.Up;

                    case ConsoleKey.DownArrow:
                    case ConsoleKey.S:
                        return Direction.Down;

                    case ConsoleKey.LeftArrow:
                    case ConsoleKey.A:
                        return Direction.Left;

                    case ConsoleKey.RightArrow:
                    case ConsoleKey.D:
                        return Direction.Right;
                }
            }
        }

        private static Direction? GetDirectionInput(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.UpArrow:
                case ConsoleKey.W:
                    return Direction.Up;

                case ConsoleKey.DownArrow:
                case ConsoleKey.S:
                    return Direction.Down;

                case ConsoleKey.LeftArrow:
                case ConsoleKey.A:
                    return Direction.Left;

                case ConsoleKey.RightArrow:
                case ConsoleKey.D:
                    return Direction.Right;
            }

            return null;
        }

        private static int DrawSelection(ConsoleBuilder builder, GameState gameState, Vector2 cursorPos, int scroll,
            int maxHeight)
        {
            //Border
            builder.ResetLineCursor();
            builder.ColumnWidth = 1;
            builder.ColumnStart = 19;

            builder.WriteLines(Enumerable.Repeat("|", maxHeight));

            //Draw Selection info
            builder.ResetLineCursor();
            builder.ColumnStart = 20;
            builder.ColumnWidth = 100;

            var tile = gameState.GetTile(cursorPos);
            var tileDescription = tile.GetDescription(gameState);

            builder.WriteLines(tileDescription.Skip(scroll).Take(maxHeight));
            return tileDescription.Length;
        }

        private static void DrawMap(ConsoleBuilder builder, GameState gameState, Vector2 cursorPos)
        {
            builder.ColumnStart = 0;
            builder.ColumnWidth = gameState.AreaSize + 1;
            for (int y = 0; y < gameState.AreaSize; y++)
            {
                for (int x = 0; x < gameState.AreaSize; x++)
                {
                    if (cursorPos == new Vector2(x, y))
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                    var tile = gameState.GetTile(x, y);
                    if (tile.Entity != null)
                    {
                        builder.Append(tile.Entity.EntitySymbol);
                    }
                    else if (tile.ResourceDrops.Any())
                    {
                        builder.Append(".");
                    }
                    else
                    {
                        builder.Append(gameState.GetTileType(x, y).MapSymbol);
                    }
                }

                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;

                //Draw right map Border
                builder.AppendLine("║");
            }

            //Bottom border
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            for (int x = 0; x < gameState.AreaSize; x++)
            {
                builder.Append("═");
            }

            builder.Append("╝");
        }
    }
}
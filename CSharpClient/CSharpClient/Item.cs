﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CSharpClient
{
    [JsonConverter(typeof(ItemConverter))]
    public abstract class Item
    {
        public string Name;

        public abstract string[] GetDescription();

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}";
        }
    }

    public class Weapon : Item
    {
        public int Attack;

        public override string[] GetDescription()
        {
            return new[]
            {
                $"Name: {Name}",
                $"  Attack: {Attack}"
            };
        }

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(Attack)}: {Attack}";
        }
    }

    public class Armor : Item
    {
        public int Defense;

        public override string[] GetDescription()
        {
            return new[]
            {
                $"Name: {Name}",
                $"  Defense: {Defense}"
            };
        }

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(Defense)}: {Defense}";
        }
    }

    public class Consumable : Item
    {
        public Skill Skill;

        public override string[] GetDescription()
        {
            var skillDescription =  Skill.GetDescription();

            for (var i = 0; i < skillDescription.Length; i++)
            {
                skillDescription[i] = $"  {skillDescription[i]}";
            }

            var description = new[]
            {
                $"Name: {Name}",
                "  Skill:"
            };

            return description.Concat(skillDescription).ToArray();
        }

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(Skill)}: {Skill}";
        }
    }

    public class ItemConverter : JsonConverter<Item>
    {
        public override void WriteJson(JsonWriter writer, Item value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override Item ReadJson(JsonReader reader, Type objectType, Item existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            var itemData = JToken.ReadFrom(reader);

            var data = itemData["item_data"];
            var type = (string)data["type"];
            Item item;
            switch (type)
            {
                case "Weapon":
                    item = new Weapon
                    {
                        Attack = (int)data["data"]["attack"],
                    };
                    break;
                case "Armor":
                    item = new Armor
                    {
                        Defense = (int)data["data"]["defense"],
                    };
                    break;
                case "Consumable":
                    item = new Consumable
                    {
                        Skill = data["data"]["skill"].ToObject<Skill>(serializer),
                    };
                    break;

                default:
                    throw new ArgumentException();
            }
            item.Name = (string)itemData["name"];
            return item;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CSharpClient
{
    [JsonConverter(typeof(EntityConverter))]
    public class Entity
    {
        public string Name;
        public string EntitySymbol;

        /// <summary>
        /// The local position within the surrounding area
        /// </summary>
        public Vector2 Pos;

        public int Health;
        public ResourceStorage Resources;
        public ResourceStorage KillReward;
        public Item[] Items;
        public Skill[] Skills;
        public EntityAction Action;

        /// <summary>
        /// The last action that was used
        /// </summary>
        public EntityAction LastAction;
        public string Errors;

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}\n" +
                   $"{nameof(Health)}: {Health}\n" +
                   $"{nameof(Items)}: {String.Join<Item>("; ", Items)}\n" +
                   $"{nameof(Skills)}: {String.Join<Skill>("; ", Skills)}\n" +
                   $"{nameof(Action)}: {Action}\n" +
                   $"{nameof(Errors)}: {Errors}";
        }

        public string[] GetDescription()
        {
            var description =  new[]
            {
                $"Name: {Name}",
                $"Health: {Health}",
                $"Action: {Action}",
                $"Last Action: {LastAction}",
            };

            var resourceDescriptions = Resources.GetDescription();
            Utilities.Indent(resourceDescriptions);

            var killRewardDescriptions = KillReward.GetDescription();
            Utilities.Indent(killRewardDescriptions);

            var itemDescriptions = Items.SelectMany(x => x.GetDescription()).ToArray();
            Utilities.Indent(itemDescriptions);

            var skillDescriptions = new List<string>();
            for (var i = 0; i < GetAllSkills().Length; i++)
            {
                var skill = GetAllSkills()[i];

                skillDescriptions.Add($"Skill {i}:");

                var skillDescription = skill.GetDescription();
                Utilities.Indent(skillDescription);
                skillDescriptions.AddRange(skillDescription);
            }

            return description
                .Append("Resources:")
                .Concat(resourceDescriptions)
                .Append("Kill Reward:")
                .Concat(killRewardDescriptions)
                .Append($"Errors: {Errors}")
                .Append("Items:")
                .Concat(itemDescriptions)
                .Concat(skillDescriptions).ToArray();
        }

        public Skill[] GetAllSkills()
        {
            return Skills
                .Concat(Items
                    .Where(x => x is Consumable)
                    .Cast<Consumable>()
                    .Select(x => x.Skill))
                .ToArray();
        }
    }

    public class EntityAction
    {
        public int SkillIndex;
        public Direction? Direction;
        public Vector2? Target;
        public int ChargeupLeft;

        public override string ToString()
        {
            return $"{nameof(SkillIndex)}: {SkillIndex}, {nameof(Direction)}: {Direction}, {nameof(Target)}: {Target}, {nameof(ChargeupLeft)}: {ChargeupLeft}";
        }
    }

    public class EntityConverter : JsonConverter<Entity>
    {
        public override void WriteJson(JsonWriter writer, Entity value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override Entity ReadJson(JsonReader reader, Type objectType, Entity existingValue, bool hasExistingValue,
            JsonSerializer serializer)
        {
            var obj = JToken.ReadFrom(reader);

            if (!obj.HasValues)
            {
                return null;
            }

            var entity = new Entity
            {
                Name = (string) obj["name"],
                EntitySymbol = (string) obj["entity_symbol"],
                Health = (int) obj["health"],
                Pos = obj["pos"].ToObject<Vector2>(serializer),
                Resources = obj["resources"].ToObject<ResourceStorage>(serializer),
                KillReward = obj["kill_reward"].ToObject<ResourceStorage>(serializer),
                Items = obj["items"].Select(x => x.ToObject<Item>(serializer)).ToArray(),
                Skills = obj["skills"].Select(x => x.ToObject<Skill>(serializer)).ToArray(),
                Action = obj["action"]?.ToObject<EntityAction>(serializer),
                LastAction = obj["last_action"]?.ToObject<EntityAction>(serializer),
                Errors = (string) obj["action_error"],
            };

            return entity;
        }
    }
}
﻿using System;

namespace CSharpClient
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ResponseAttribute : Attribute
    {
        public string ResponseName;

        public ResponseAttribute(string responseName)
        {
            ResponseName = responseName;
        }
    }

    [Response("PlayerAccepted")]
    public class PlayerAccepted
    {
        public string Key;

        public override string ToString()
        {
            return $"{nameof(Key)}: {Key}";
        }
    }
}